#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2019 jaarac <jaarac@rog>
#
# Distributed under terms of the MIT license.


"""

"""


import tensorflow as tf

mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

ts = x_train.shape
x_train = x_train.reshape(ts[0], ts[1], ts[2], 1)
tt = x_test.shape
x_test = x_test.reshape(tt[0], tt[1], tt[2], 1)

model = tf.keras.models.Sequential([
  tf.keras.layers.Conv2D(4, (5, 5), activation='relu',
                         input_shape=(28, 28, 1)),
  tf.keras.layers.Conv2D(8, (4, 4), strides=(2, 2), activation='relu'),
  tf.keras.layers.Conv2D(12, (4, 4), strides=(2, 2), activation='relu'),
  tf.keras.layers.Flatten(),
  tf.keras.layers.Dense(200, activation='relu'),
  tf.keras.layers.Dense(10, activation='softmax')
])

optimizer = tf.keras.optimizers.Adam(decay=.0001)

model.compile(optimizer=optimizer,
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=5)

model.evaluate(x_test, y_test)
